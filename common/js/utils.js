// https://github.com/ineo6/hubot-dingtalk/blob/master/dingtalk.js
// https://github.com/ineo6/dingtalk-robot-sdk/blob/master/src/robot.js
const DINGTALK_ACCESS_TOKEN = 'c10fd5991b46481142648156bee6dbf48981277a7c6bc803b168f14f047673cc'
const DINGTALK_SECRET = 'SEC0fde189fef95beb0d23a5469cccfc74c9d0da70b104cdd256cafa2a31fb7b723'
export const ding = (msg) => {
	const timestamp = Date.now()
	const crypto = require('crypto')
	// 加签
	const sign = encodeURIComponent(crypto.createHmac('sha256', DINGTALK_SECRET).update(timestamp + '\n' + DINGTALK_SECRET).digest('base64'))
	uni.request({
		url: 'https://oapi.dingtalk.com/robot/send?access_token=' + DINGTALK_ACCESS_TOKEN + '&timestamp=' + timestamp + '&sign=' + sign,
		method: 'POST',
		header: {
			'Content-Type': 'application/json'
		},
		data: JSON.stringify({
			msgtype: 'text',
			text: {
				content: msg + ' [源自分享小程序]'
			}
		})
	})
}

// 防抖
let timeout = null
export const debounce = (fn, wait) => {
  if (timeout !== null) { clearTimeout(timeout) }
  timeout = setTimeout(fn, wait)
}