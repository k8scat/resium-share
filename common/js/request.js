import Request from '@/js_sdk/luch-request/luch-request/index.js'

const http = new Request({
	baseURL: 'https://api.resium.ncucoder.com/',
	timeout: 30000
});

http.interceptors.response.use((response) => { /* 对响应成功做点什么 可使用async await 做异步操作*/
	const code = response.data.code
	if (code !== 200) {
		uni.showToast({
			title: response.data.msg,
			icon: 'none'
		})
		return Promise.reject(response)
	}
	return response.data
}, (response) => { /*  对响应错误做点什么 （statusCode !== 200）*/
	uni.showToast({
		title: '接口请求失败',
		icon: 'none'
	})
	return Promise.reject(response)
})

export default http